# ai.io

## setup
```sh
# clone
git clone https://gitlab.com/florianlprt/ai.io.git

# install
cd ai.io/
npm install

# run (prod)
npm start

# run (dev)
npm run devstart
```

## Demo

![](public/videos/aiio.mp4)
