const fs = require('fs').promises;

const updater = require('../bridges/updater');

/*******************************************************************************
 * Home controllers
 */

// Index
exports.index = function(req, res, next) {
  res.redirect('/home');
};

// Home
exports.home = function(req, res, next) {
  res.render('layout', {
    title: 'Home',
    page: 'home'
  });
};

/*******************************************************************************
 * Play controllers
 */

// Play
exports.play = function(req, res, next) {
  res.render('layout', {
    title: 'Play',
    page: 'play',
    games: updater.games,
    players: updater.players
  });
};

// TODO: Prepare
exports.prepare = function(req, res, next) {
  console.log('Prepare', req.body);
  next();
};

// Board
exports.board = function(req, res, next) {
  const { game, player1, player2 } = req.body;
  res.render('layout', {
    title: 'Board',
    page: 'board',
    game,
    player1,
    player2
  });
};

/*******************************************************************************
 * Code controllers
 */

// Code
exports.code = function(req, res, next) {
  res.render('layout', {
    title: 'Code',
    page: 'code'
  });
};

// TODO: Check
exports.check = function(req, res, next) {
  console.log('Check', req.body);
  next();
};

// Upload
exports.upload = async function(req, res, next) {
  const { playerKey, fullName, fullCode } = req.body;
  await updater.upload(playerKey, fullName, fullCode);
  res.send('Upload success');
};

/*******************************************************************************
 * League controllers
 */

// League
exports.league = async function(req, res, next) {
  const { games, lock } = updater;
  const leagues = {};
  for (let game in games) {
    const jsonLeague = await fs.readFile(`data/leagues/${game}.json`, 'utf-8');
    const league = JSON.parse(jsonLeague);
    leagues[game] = league;
  }
  res.render('layout', {
    title: 'League',
    page: 'league',
    games,
    leagues,
    lock
  });
};

// Update
exports.update = function(req, res, next) {
  const { game } = req.params;
  if (!updater.lock[game]) {
    updater.update(game);
  }
  res.redirect('/league');
};