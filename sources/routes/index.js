var express = require('express');
var controller = require('../controllers/index');

var router = express.Router();

/* GET index. */
router.get('/', controller.index);

/* GET home. */
router.get('/home', controller.home);

/* GET play. */
router.get('/play', controller.play);

/* GET league. */
router.get('/league', controller.league);

/* GET update. */
router.get('/update/:game', controller.update);

/* GET code. */
router.get('/code', controller.code);

/* POST board. */
router.post('/board', controller.prepare, controller.board);

/* POST upload. */
router.post('/upload', controller.check, controller.upload);

module.exports = router;
