import fs from 'fs';
import worker from 'worker_threads';

const PLAYERS = fs.readFileSync('data/players.json', 'utf-8');
const GAMES = fs.readFileSync('data/games.json', 'utf-8');

class Updater {
  constructor() {
    this.lock = {};
    this.matches = 100;
    this.games = JSON.parse(GAMES);
    this.players = JSON.parse(PLAYERS);
  }

  get battles() {
    const players = Object.keys(this.players);
    const battles = [];
    for (let first=0; first<players.length-1; first++) {
      for (let second=first+1; second<players.length; second++) {
        battles.push({
          player1: players[first],
          player2: players[second]
        });
      }
    }
    return battles;
  }

  compete(game) {
    return new Promise((resolve, reject) => {
      const matcher = new worker.Worker('./sources/modules/matcher.mjs', {
        type: 'module',
        workerData: {
          game,
          matches: this.matches,
          battles: this.battles
        }
      });
      matcher.on('online', () => { console.log('Worker updating:', game) });
      matcher.on('message', message => resolve(message));
      matcher.on('error', reject);
      matcher.on('exit', code => {
        if (code !== 0) {
          reject(new Error(`Worker stopped with exit code ${code}`));
        }
      });
    });
  }

  winrates(results) {
    const totalPlayers = Object.keys(this.players).length;
    const totalMatchesRatio = (this.matches * (totalPlayers - 1)) / 100;
    const winrates = {};
    for (let result of results) {
      for (let player in result) {
        winrates[player] = (winrates[player] || 0) + result[player];
      }
    }
    for (let player in winrates) {
      winrates[player] /= totalMatchesRatio;
    }
    return winrates;
  }

  sort(winrates) {
    const data = {};
    for (let player in winrates) {
      const row = {
        name: this.players[player],
        winrate: winrates[player].toFixed(2)
      };
      data[player] = row;
    }
    return data;
  }

  async update(game) {
    if (game in this.games) {
      this.lock[game] = true;
      const results = await this.compete(game);
      const winrates = this.winrates(results);
      const data = this.sort(winrates);
      fs.writeFile(`data/leagues/${game}.json`, JSON.stringify(data),
        (err) => {
          if (err) throw err;
          this.lock[game] = false;
        }
      );
    }
  }

  async upload(playerKey, fullName, fullCode) {
    this.players[playerKey] = fullName;
    fs.writeFile('data/players.json', JSON.stringify(this.players),
      (err) => {
        if (err) throw err;
      }
    );
    fs.writeFile(`public/javascripts/players/${playerKey}.mjs`, fullCode,
      (err) => {
        if (err) throw err;
      }
    );
  }
}

export default new Updater();