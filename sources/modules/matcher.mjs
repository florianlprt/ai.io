import { parentPort, workerData } from 'worker_threads';

function battle(Game, Player1, Player2, Manager) {
  const game = new Game();
  const player1 = new Player1(game, 1);
  const player2 = new Player2(game, 2);
  const manager = new Manager(game, player1, player2);
  manager.start();
  return manager.game.winner;
}

async function run(game, player1, player2, matches) {
  const Game = await import(`../../public/javascripts/games/${game}.mjs`);
  const Player1 = await import(`../../public/javascripts/players/${player1}.mjs`);
  const Player2 = await import(`../../public/javascripts/players/${player2}.mjs`);
  const Manager = await import(`../../public/javascripts/managers/remote.mjs`);
  const results = {
    [player1]: 0,
    [player2]: 0
  };
  for (let match=0; match<matches; match++) {
    const winner = battle(
      Game.default, Player1.default, Player2.default, Manager.default
    );
    if (winner === 1) {
      results[player1] += 1;
    }
    if (winner === 2) {
      results[player2] += 1;
    }
  }
  return results;
}

async function initialize(game, matches, battles) {
  const results = await Promise.all(
    battles.map(
      async ({ player1, player2 }) => await run(game, player1, player2, matches)
    )
  );
  console.log('Worker finished:', game);
  parentPort.postMessage(results);
}

const { game, matches, battles } = workerData;
initialize(game, matches, battles);