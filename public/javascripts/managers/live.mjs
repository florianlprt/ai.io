import Manager from '../engine/manager.mjs';

class Live extends Manager {
  constructor(game, player1, player2) {
    super(game, player1, player2);
  }

  /**
   * Following methods need to be implemented by the manager.
   */
  start() {
    this.next();
  }

  move(move) {
    if (!this.game.isOver && this.game.possibleMoves.includes(move)) {
      this.game.move(move);
      this.game.nextTurn();
      this.next();
      return true;
    }
    return false;
  }

  next() {
    if (this.player.isAI) {
      const move = this.player.move();
      this.move(move);
    }
  }
}

export default Live;