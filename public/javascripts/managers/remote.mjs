import Manager from '../engine/manager.mjs';

class Remote extends Manager {
  constructor(game, player1, player2) {
    super(game, player1, player2);
  }

  /**
   * Following methods need to be implemented by the manager.
   */
  start() {
    this.next();
  }

  move(move) {
    if (this.game.isOver) {
      return;
    }
    if (this.game.possibleMoves.includes(move)) {
      this.game.move(move);
      this.game.nextTurn();
      this.next();
    }
    else {
      throw `Player ${this.game.turn} failed to move`;
    }
  }

  next() {
    const move = this.player.move();
    this.move(move);
  }
}

export default Remote;