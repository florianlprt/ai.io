class Player {
  constructor(game, turn, name, isAI) {
    this.game = game;
    this.turn = turn;
    this.name = name;
    this.isAI = isAI;
  }

  /**
   * Following methods need to be implemented by the player.
   */
  move() {
    throw 'NotImplemented';
  }
}

export default Player;