class Manager {
  constructor(game, player1, player2) {
    this.game = game;
    this.player1 = player1;
    this.player2 = player2;
  }

  get player() {
    return this.game.turn === 1 ? this.player1 : this.player2;
  }

  /**
   * Following methods need to be implemented by the manager.
   */
  start() {
    throw 'NotImplemented';
  }

  move() {
    throw 'NotImplemented';
  }

  nextTurn() {
    throw 'NotImplemented';
  }
}

export default Manager;