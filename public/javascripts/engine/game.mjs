class Game {
  constructor(name) {
    this.name = name;
    this.board = [];
    this.turn = 1;
    this.initialize();
  }

  get isOver() {
    return this.winner || this.possibleMoves.length === 0;
  }

  nextTurn() {
    this.turn = this.turn === 1 ? 2 : 1;
  }

  /**
   * Following methods need to be implemented by the game.
   */
  get possibleMoves() {
    throw 'NotImplemented';
  }

  get winner() {
    throw 'NotImplemented';
  }

  clone() {
    throw 'NotImplemented';
  }

  initialize() {
    throw 'NotImplemented';
  }

  move(move) {
    throw 'NotImplemented';
  }
}

export default Game;