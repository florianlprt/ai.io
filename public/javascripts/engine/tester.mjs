import TicTacToe from '../games/tictactoe.mjs';
import Player from '../engine/player.mjs';
import Random from '../players/random.mjs';
import Remote from '../managers/remote.mjs';

class Tester {
  constructor() {
    this.games = 10;
    this.code = null;
    this.playerKey = null;
    this.playerClass = null;
    this.playerClassName = null;
  }

  get infos() {
    const player = new this.playerClass();
    const fullName = player.name;
    const fullCode = `
import Player from '../engine/player.mjs';
${this.code}
export default ${this.playerClassName};
    `;
    return { playerKey: this.playerKey, fullName, fullCode };
  }

  parse(code) {
    const firstLine = code.split('\n')[0];
    const className = firstLine.split(' ')[1];
    const player = eval(code + '\n' + className);
    this.code = code;
    this.playerKey = className.toLowerCase();
    this.playerClass = player;
    this.playerClassName = className;
  }

  test() {
    for (let g=0; g<this.games; g++) {
      const game = new TicTacToe();
      const player1 = new this.playerClass(game, 1);
      const player2 = new Random(game, 2);
      const manager = new Remote(game, player1, player2);
      try {
        manager.start();
      } 
      catch (error) {
        return false;
      }
    }
    return true;
  }
}

export default Tester;