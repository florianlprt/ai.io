import Player from '../engine/player.mjs';

class Human extends Player {
  constructor(game, turn) {
    super(game, turn, 'Human', false);
  }
}

export default Human;