import Player from '../engine/player.mjs';

class Random extends Player {
  constructor(game, turn) {
    super(game, turn, 'Random', true);
  }
  
  /**
   * Following methods need to be implemented by the player.
   */
  move() {
    const moves = this.game.possibleMoves;
    return moves[Math.floor(Math.random() * moves.length)];
  }
}

export default Random;