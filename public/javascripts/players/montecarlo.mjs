import Player from '../engine/player.mjs';

class MonteCarlo extends Player {
  constructor(game, turn) {
    super(game, turn, 'Monte Carlo', true);
    this.simulations = 20;
  }

  simulate(move) {
    const game = this.game.clone();
    game.move(move);
    game.nextTurn();
    while (!game.isOver) {
      const moves = game.possibleMoves;
      const move = moves[Math.floor(Math.random() * moves.length)];
      game.move(move);
      game.nextTurn();
    }
    return game.winner === this.turn ? 1 : 0;
  }
  
  /**
   * Following methods need to be implemented by the player.
   */
  move() {
    const moves = this.game.possibleMoves;
    const rewards = Array(moves.length).fill(0);
    moves.forEach((move, index) => {
      for (let sim=0; sim<this.simulations; sim++) {
        rewards[index] += this.simulate(move);
      }
    });
    return moves[rewards.indexOf(Math.max(...rewards))];
  }
}

export default MonteCarlo;