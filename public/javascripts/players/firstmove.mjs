import Player from '../engine/player.mjs';

class FirstMove extends Player {
  constructor(game, turn) {
    super(game, turn, 'First Move', true);
  }
  
  /**
   * Following methods need to be implemented by the player.
   */
  move() {
    return this.game.possibleMoves[0];
  }
}

export default FirstMove;