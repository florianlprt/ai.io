import Game from '../engine/game.mjs';

class TicTacToe extends Game {
  constructor() {
    super('Tic Tac Toe');
  }

  /**
   * Following methods need to be implemented by the game.
   */  
  get possibleMoves() {
    return this.board
      .map((_, index) => index)
      .filter(index => this.board[index] === 0);
  }

  get winner() {
    const board = this.board.slice();
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (board[a] && board[a] === board[b] && board[a] === board[c]) {
        return board[a];
      }
    }
    return 0;
  }

  clone() {
    const game = new TicTacToe();
    game.turn = this.turn;
    game.board = this.board.slice();
    return game;
  }

  initialize() {
    this.board = Array(9).fill(0);
    this.turn = Math.random() < .5 ? 1 : 2;
  }

  move(move) {
    this.board[move] = this.turn;
  }
}

export default TicTacToe;