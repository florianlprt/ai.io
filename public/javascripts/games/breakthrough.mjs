import Game from '../engine/game.mjs';

class Breakthrough extends Game {
  constructor() {
    super('Breakthrough');
  }

  i2xy(index) {
    return { x: parseInt(index / 8), y: index % 8 };
  }

  xy2i(x, y) {
    return x * 8 + y;
  }

  /**
   * Following methods need to be implemented by the game.
   */  
  get possibleMoves() {
    const pieces = this.board
      .map((_, index) => index)
      .filter(index => this.board[index] === this.turn);
    const direction = this.turn === 1 ? -1 : 1;
    const moves = [];
    pieces.forEach(piece => {
      const { y } = this.i2xy(piece);
      // Left
      if (y - 1 >= 0) {
        const left = piece + direction * (this.turn === 1 ? 9 : 7);
        if (this.board[left] !== this.board[piece]) {
          moves.push(`${piece},${left}`);
        }
      }
      // Front
      const front = piece + direction * 8;
      if (this.board[front] === 0) {
        moves.push(`${piece},${front}`);
      }
      // Right
      if (y + 1 < 8) {
        const right = piece + direction * (this.turn === 1 ? 7 : 9);
        if (this.board[right] !== this.board[piece]) {
          moves.push(`${piece},${right}`);
        }
      }
    });
    return moves;
  }

  get winner() {
    // Player1 broke through
    for (let square=0; square<8; square++) {
      if (this.board[square] === 1) {
        return 1;
      }
    }
    // Player2 broke through
    for (let square=56; square<64; square++) {
      if (this.board[square] === 2) {
        return 2;
      }
    }
    // Player1 captured everything
    if (this.board.filter(piece => piece === 2).length === 0) {
      return 1;
    }
    // Player2 captured everything
    if (this.board.filter(piece => piece === 1).length === 0) {
      return 2;
    }
  }

  clone() {
    const game = new Breakthrough();
    game.turn = this.turn;
    game.board = this.board.slice();
    return game;
  }

  initialize() {
    this.turn = 1;
    this.board = [].concat(
      Array(16).fill(2),
      Array(32).fill(0),
      Array(16).fill(1)
    );
  }

  move(move) {
    const [from, to] = move.split(',');
    this.board[parseInt(from)] = 0;
    this.board[parseInt(to)] = this.turn;
  }
}

export default Breakthrough;