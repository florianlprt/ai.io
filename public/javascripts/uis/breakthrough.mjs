import Game from './game.mjs';

class Breakthrough extends Game {
  constructor(game, manager) {
    super(game, manager);
    this.isSelected = false;
    this.display = this.calculateDisplay();
  }

  selectedColor(player) {
    return player === 1 ? '#0062cc' : '#bd2130';
  }

  calculateDisplay() {
    const { width } = this.canvas;
    const margin = 20;
    const padding = 1;
    const eighth = width / 8;
    const maxSquare = (width - 2 * margin) / 8;
    const minSquare = maxSquare - 2 * padding;
    const radius = minSquare * .4;
    return { margin, padding, eighth, maxSquare, minSquare, radius };
  }

  drawSelection(piece, index) {
    const { ctx } = this.canvas;
    const { margin, padding, maxSquare, minSquare, radius } = this.display;
    const { x: row, y: col } = this.game.i2xy(index);
    ctx.beginPath();
    ctx.arc(
      margin + col * maxSquare + padding + minSquare / 2,
      margin + row * maxSquare + padding + minSquare / 2,
      radius,
      0,
      2 * Math.PI,
      false
    );
    ctx.fillStyle = this.selectedColor(piece);
    ctx.fill();
  }

  /**
   * Following methods need to be implemented by the user interface.
   */
  click(e) {
    if (this.manager.player.isAI) {
      return;
    }
    // Measures
    const { width, height } = this.canvas;
    const { margin, padding, eighth, maxSquare } = this.display;
    // In board ?
    const { offsetX: x, offsetY: y } = e;
    if (x < margin || x > width - margin
        || y < margin || y > height - margin) {
      return;
    }
    // In minSquare ?
    const xs = (x - margin) % maxSquare;
    const ys = (y - margin) % maxSquare;
    if (xs < padding || xs > maxSquare - padding
        || ys < padding || ys > maxSquare - padding) {
      return;
    }
    // Move
    const move = parseInt((x - margin) / maxSquare) +
                 parseInt((y - margin) / maxSquare) * 8;
    if (this.isSelected !== false) {
      this.manager.move(this.isSelected + ',' + move);
      this.update();
      this.isSelected = false;
    }
    else {
      const piece = this.game.board[move];
      if (piece === this.game.turn) {
        this.isSelected = move;
        this.drawSelection(piece, move);
      }
    }
  }

  drawBoard() {
    // Measures
    const { ctx, width, height } = this.canvas;
    const { margin, padding, maxSquare, minSquare, radius } = this.display;
    // Clear
    ctx.clearRect(0, 0, width, height);
    // Draw
    this.game.board.forEach((piece, index) => {
      // Index
      const { x: row, y: col } = this.game.i2xy(index);
      // Squares
      ctx.fillStyle = '#ddd';
      ctx.fillRect(
        margin + col * maxSquare + padding,
        margin + row * maxSquare + padding,
        minSquare,
        minSquare
      );
      // Piece
      if (piece) {
        ctx.beginPath();
        ctx.arc(
          margin + col * maxSquare + padding + minSquare / 2,
          margin + row * maxSquare + padding + minSquare / 2,
          radius,
          0,
          2 * Math.PI,
          false
        );
        ctx.fillStyle = this.playerColor(piece);
        ctx.fill();
      }
    });
  }
}

export default Breakthrough;