const Feedback = {};

const upload = document.querySelector('#upload');
const unittest = document.querySelector('#unittest');

Feedback.reset = () => {
  upload.disabled = true;
  unittest.innerHTML = '<i class="fas fa-play"></i> Run tests';
  unittest.classList.remove('btn-outline-danger', 'btn-outline-success');
  unittest.classList.add('btn-outline-secondary');
};

Feedback.uploadSuccess = () => {
  upload.disabled = true;
  upload.innerHTML = '<i class="fas fa-check"></i> Upload success';
  upload.classList.remove('btn-outline-secondary');
  upload.classList.add('btn-outline-success');
};

Feedback.uploadWait = () => {
  upload.disabled = true;
  upload.innerHTML = '<i class="fas fa-spin fa-sync-alt"></i> Uploading';
  unittest.disabled = true;
};

Feedback.unittestSuccess = () => {
  upload.disabled = false;
  unittest.innerHTML = '<i class="fas fa-check"></i> Tests check';
  unittest.classList.remove('btn-outline-secondary');
  unittest.classList.add('btn-outline-success');
};

Feedback.unittestFail = () => {
  unittest.classList.remove('btn-outline-secondary');
  unittest.classList.add('btn-outline-danger');
};

export default Feedback;