import Game from './game.mjs';

class TicTacToe extends Game {
  constructor(game, manager) {
    super(game, manager);
    this.display = this.calculateDisplay()
  }

  calculateDisplay() {
    const { width } = this.canvas;
    const margin = 20;
    const padding = 2;
    const third = width / 3;
    const maxSquare = (width - 2 * margin) / 3;
    const minSquare = maxSquare - 2 * padding;
    const radius = minSquare * .45;
    return { margin, padding, third, maxSquare, minSquare, radius };
  }

  /**
   * Following methods need to be implemented by the user interface.
   */
  click(e) {
    // Measures
    const { width, height } = this.canvas;
    const { margin, padding, third, maxSquare } = this.display;
    // In board ?
    const { offsetX: x, offsetY: y } = e;
    if (x < margin || x > width - margin
        || y < margin || y > height - margin) {
      return;
    }
    // In minSquare ?
    const xs = (x - margin) % maxSquare;
    const ys = (y - margin) % maxSquare;
    if (xs < padding || xs > maxSquare - padding
        || ys < padding || ys > maxSquare - padding) {
      return;
    }
    // Move
    const move = parseInt((x - margin) / maxSquare) +
                 parseInt((y - margin) / maxSquare) * 3;
    if (!this.manager.player.isAI && this.manager.move(move)) {
      this.update();
    }
  }

  drawBoard() {
    // Measures
    const { ctx, width, height } = this.canvas;
    const { margin, padding, maxSquare, minSquare, radius } = this.display;
    // Clear
    ctx.clearRect(0, 0, width, height);
    // Draw
    this.game.board.forEach((piece, index) => {
      const row = parseInt(index / 3);
      const col = index % 3;
      // Squares
      ctx.fillStyle = '#ddd';
      ctx.fillRect(
        margin + col * maxSquare + padding,
        margin + row * maxSquare + padding,
        minSquare,
        minSquare
      );
      // Piece
      if (piece) {
        ctx.beginPath();
        ctx.arc(
          margin + col * maxSquare + padding + minSquare / 2,
          margin + row * maxSquare + padding + minSquare / 2,
          radius,
          0,
          2 * Math.PI,
          false
        );
        ctx.fillStyle = this.playerColor(piece);
        ctx.fill();
      }
    });
  }
}

export default TicTacToe;