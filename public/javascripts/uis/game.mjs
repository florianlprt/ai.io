class Game {
  constructor(game, manager) {
    this.game = game;
    this.manager = manager;
    this.dom = {
      board: document.querySelector('#board'),
      game: document.querySelector('#game'),
      status: document.querySelector('#status'),
      restart: document.querySelector('#restart')
    };
    this.canvas = {
      ctx: this.dom.board.getContext('2d'),
      width: this.dom.board.width,
      height: this.dom.board.height
    };
    this.initializeEvents();
    this.initializeStatus();
  }

  playerColor(player) {
    return player === 1 ? '#007bff' : '#dc3545';
  }

  initializeEvents() {
    this.dom.board.onclick = (e) => this.click(e);
    this.dom.restart.onclick = () => this.restart();
  }

  initializeStatus() {
    this.dom.game.innerText = this.game.name;
    this.dom.status.innerText = 'Game is loading...';
  }

  playerStatus() {
    const isOver = this.game.isOver;
    if (isOver === true) {
      this.dom.status.innerHTML = `<i class="fas fa-dizzy text-secondary"></i> Tie !`;
    }
    else if (isOver) {
      this.dom.status.innerHTML = `
        <i class="fas fa-trophy" style="color:${this.playerColor(isOver)}"></i>
        Player ${isOver} wins !
      `;
    }
    else {
      const dice = this.game.turn === 1 ? 'one' : 'two';
      this.dom.status.innerHTML = `
        <i class="fas fa-dice-${dice}" style="color:${this.playerColor(this.game.turn)}"></i>
        Player ${this.game.turn} is playing.
      `;
    }
  }

  restart() {
    this.game.initialize();
    this.manager.start();
    this.update();
  }

  update() {
    this.drawBoard();
    this.playerStatus();
  }

  /**
   * Following methods need to be implemented by the user interface.
   */
  click(e) {
    throw 'NotImplemented';
  }

  drawBoard() {
    throw 'NotImplemented';
  }
}

export default Game;